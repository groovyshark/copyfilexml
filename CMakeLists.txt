cmake_minimum_required(VERSION 3.7)
project(CopyFileXML)

set(CMAKE_CXX_STANDARD 14)

set(SOURCE_FILES main.cpp pugixml.hpp pugixml.cpp pugiconfig.hpp config.h)
add_executable(CopyFileXML ${SOURCE_FILES})