//
// Created by grand on 8/11/17.
//

#pragma once

#include <cerrno>
#include <getopt.h>
#include <sstream>
#include <memory.h>

/* ...command-line options */
static const struct option    options[] = {
        {   "in",               required_argument, nullptr,   'i' },
        {   "help",             no_argument,       nullptr,     1 },

        {   nullptr,               0,              nullptr,     0 },
};

std::string help()
{
    static std::stringstream ss;
    ss << " Help information and arguments description : " << std::endl
       << "    -i    --in           input configuration name *.xml (e.g. config.xml ) " << std::endl;

    return ss.str();
}