#include <iostream>
#include <fstream>

#include "config.h"
#include "pugixml.hpp"

int copyfile(const std::string& srcpath, const std::string& dstpath);


int main(int argc, char* argv[])
{
    std::string configFile;

    int opt, index = 0;
    while ((opt = getopt_long(argc, argv, "i:", options, &index)) != -1)
    {
        switch (opt)
        {
            case 'i':
                configFile = strdup((std::string(optarg)).c_str());
                break;
            case 1:
                std::cout << help();
                return 0;
            default:
                return -EINVAL;
        }
    }

    /// Load XML file
    pugi::xml_document doc;
    auto result = doc.load_file(configFile.c_str());

    if (!result)
    {
        std::cerr << "XML [" << configFile << "] parsed with errors"
                  << "Error description: " << result.description() << std::endl;

        return -1;
    }

    /// Parse XML-file by attributes and copy file from src to dst
    pugi::xml_node tools = doc.child("config").child("filearray");
    for (auto item : tools.children())
    {
        std::string name    = item.attribute("name").value();
        std::string src     = std::string(item.attribute("srcpath").value()) + "/" + name;
        std::string dst     = std::string(item.attribute("dstpath").value()) + "/" + name;

        if (copyfile(src, dst) > 0)
            std::cout << "Copy complete: "<< src << " -> "<< dst;
        else
            std::cerr << "Can't copy file: "<< src;

        std::cout << std::endl;
    }

    return 0;
}

int copyfile(const std::string& srcpath, const std::string& dstpath)
{
    std::ifstream  src(srcpath, std::ios::binary);
    std::ofstream  dst(dstpath, std::ios::binary);

    if (!src.is_open() || !dst.is_open())
    {
        std::cerr << "Can't open file: "<< srcpath << std::endl;
        return -1;
    }

    std::streambuf* filebuf = src.rdbuf();
    dst << filebuf;

    return static_cast <int> (filebuf->pubseekoff(0, src.end));
}